DROP DATABASE IF EXISTS shop;
CREATE DATABASE shop;
USE shop;

CREATE TABLE category (
id INT AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(20), 
description TEXT
);

CREATE TABLE product(
id INT AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(20), 
price DECIMAL(6,2), 
category_id INT,
FOREIGN KEY (category_id) REFERENCES category(id)
);


INSERT INTO category VALUES
(id, "Warzywa", "Promocja 50%"),
(id, "Owoce", "1 gratis przy zakupie 3"),
(id, "Mięso", "Przeterminowane mięso rabat 10%");

INSERT INTO product VALUES
(id, "Ziemniak", 10, 1),
(id, "Marchew", 2, 1),
(id, "Groszek", 1, 1),
(id, "Ogórek", 3, 1),
(id, "Jabłko", 1, 2),
(id, "Gruszka", 1, 2),
(id, "Arbuz", 10, 2),
(id, "Wieprzowina", 15, 3),
(id, "Wołowina", 25, 3),
(id, "Cielęcina", 32, 3);

SELECT p.name FROM product p;

SELECT p.name, c.name FROM product p LEFT JOIN category c ON p.category_id = c.id;

CREATE VIEW category_name AS SELECT name FROM category;

SELECT * FROM category_name;

CREATE VIEW product_full AS
SELECT 
  p.name as product_name, 
  p.price,
  c.name as category_name
FROM product p 
LEFT JOIN category c 
ON p.category_id = c.id;

SELECT * FROM product_full;
SELECT count(*) as product_count, category_name FROM product_full GROUP BY category_name;
SELECT avg(price) as avg_price, category_name FROM product_full GROUP BY category_name;

SELECT * FROM product_fu