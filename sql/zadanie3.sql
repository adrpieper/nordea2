
DROP DATABASE IF EXISTS company;

CREATE DATABASE company;

USE company;

DROP TABLE IF EXISTS worker;

CREATE TABLE worker (
id INT AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(32),
salary DECIMAL(8,2),
age INT
);

INSERT INTO worker (name,salary,age) VALUES ("Jan", 1000, 25);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 1000, 20);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 1000, 25);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 1500, 20);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 1500, 20);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 1500, 25);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 2000, 20);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 2000, 25);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 4000, 20);
INSERT INTO worker (name,salary,age) VALUES ("Jan", 4000, 25);

SELECT * FROM worker;

SELECT * FROM worker ORDER BY salary;
SELECT * FROM worker ORDER BY salary DESC;

SELECT count(*) as workers_count, salary FROM worker GROUP BY salary;
SELECT avg(salary) as avg_salary, age FROM worker GROUP BY age;


SELECT * FROM worker ORDER BY salary LIMIT 1;
SELECT * FROM worker ORDER BY salary DESC LIMIT 1;

SELECT * FROM worker WHERE salary = (SELECT max(salary) FROM worker);

