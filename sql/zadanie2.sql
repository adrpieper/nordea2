
DROP DATABASE IF EXISTS contact_data;
CREATE DATABASE contact_data;
USE contact_data;

CREATE TABLE contact (
  id int AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(32),
  last_name VARCHAR(32),
  number VARCHAR(12),
  e_mail VARCHAR(32)
);

INSERT INTO contact (first_name, last_name, number) VALUES
("Jan", "Kowalski", "000-102-105"),
("Adam", "Kowalski", "100-102-103"),
("Dominik", "Kowalski", "200-102-113"),
("Jan", "Nowak", "300-102-103"),
("Adam", "Nowak", "400-102-103");

SELECT * FROM contact;
SELECT last_name, number FROM contact;
SELECT concat(first_name," ",last_name) as person, e_mail FROM contact;
SELECT concat(first_name," ",last_name) as person, e_mail FROM contact WHERE id = 3;
UPDATE contact SET last_name = NULL WHERE id = 3;

SELECT count(*) FROM contact;


SELECT * FROM contact WHERE last_name = "Kowalski";
SELECT * FROM contact WHERE number LIKE "400%";
SELECT * FROM contact WHERE number LIKE "%0";
UPDATE contact SET e_mail = "Brak" WHERE e_mail IS NULL;