DROP DATABASE IF EXISTS personal_data;

CREATE DATABASE personal_data;
USE personal_data;


CREATE TABLE person (
	first_name VARCHAR(32),
	last_name VARCHAR(32),
	age TINYINT UNSIGNED
) DEFAULT
CHARACTER SET utf8 COLLATE utf8_polish_ci;


INSERT INTO person VALUES 
("Jan", "Kowalski", 21),
("Adam", "Kowalski", 22),
("Dominik", "Kowalski", 22),
("Jan", "Nowak", 24),
("Adam", "Nowak", 24);

SET SQL_SAFE_UPDATES = 0;

SELECT * FROM person;
SELECT first_name FROM person;

UPDATE person SET age = age + 1;

SELECT count(*) as person_count, age FROM person GROUP BY age;


