DROP DATABASE IF EXISTS shop;
CREATE DATABASE shop;
USE shop;

CREATE TABLE category (
id INT AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(20), 
description TEXT
);

CREATE TABLE product(
id INT AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(20), 
price DECIMAL(6,2), 
category_id INT,
FOREIGN KEY (category_id) REFERENCES category(id)
);

INSERT INTO category VALUES
(id, "Warzywa", "Promocja 50%"),
(id, "Owoce", "1 gratis przy zakupie 3"),
(id, "Mięso", "Przeterminowane mięso rabat 10%");

INSERT INTO product VALUES
(id, "Ziemniak", 10, 1),
(id, "Marchew", 2, 1),
(id, "Groszek", 1, 1),
(id, "Ogórek", 3, 1),
(id, "Jabłko", 1, 2),
(id, "Gruszka", 1, 2),
(id, "Arbuz", 10, 2),
(id, "Wieprzowina", 15, 3),
(id, "Wołowina", 25, 3),
(id, "Cielęcina", 32, 3);

CREATE TABLE customer (
id INT AUTO_INCREMENT PRIMARY KEY,
   first_name VARCHAR(30),
   last_name VARCHAR(30)
   );
   
CREATE TABLE purchase (
   id INT AUTO_INCREMENT PRIMARY KEY,
   date DATE,
   paid ENUM ('NEW', 'PAID', 'CANCELED', 'COMPLETION'),
   customer_id INT, 
   FOREIGN KEY (customer_id) REFERENCES customer(id)
   );
   
CREATE TABLE purchase_products (
   purchase_id INT, 
   product_id INT, 
   quantity INT UNSIGNED NOT NULL ,
   FOREIGN KEY (purchase_id) REFERENCES purchase(id),
   FOREIGN KEY (product_id) REFERENCES product(id),
   PRIMARY KEY (purchase_id,product_id)
   );
   
INSERT INTO customer VALUES (id, "Jan", "Kowalski"), (id, "Adam", "Nowak");

INSERT INTO purchase VALUES (id, "2017-03-06", 'NEW', 1);
INSERT INTO purchase VALUES (id, "2017-03-07", 'NEW', 1);

INSERT INTO purchase_products VALUES 
	(1,1,10),
	(1,2,1),
	(1,3,4),
	(1,4,6),
	(1,5,20),
	(2,2,3),
	(2,3,10),
	(2,4,40),
	(2,5,2),
	(2,6,3);
    
