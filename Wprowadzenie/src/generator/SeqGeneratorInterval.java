package generator;

/**
 * Created by RENT on 2017-07-22.
 */
public class SeqGeneratorInterval {

    private int counter;
    private int max;

    public SeqGeneratorInterval(int min, int max) {
        this.counter = min;
        this.max = max;
    }

    public Integer generate(){
        if(counter <max){
            return counter++;
        }
        return null;
    }
    public static void main(String[] args) {
        SeqGeneratorInterval seqGeneratorInterval = new SeqGeneratorInterval(5,12);
        for (int i = 0; i < 10; i++) {
            System.out.println(seqGeneratorInterval.generate());
        }
    }
}
