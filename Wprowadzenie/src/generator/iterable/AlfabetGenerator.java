package generator.iterable;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-07-22.
 */
public class AlfabetGenerator implements Iterable<Character> {
    public static final char BEFORE_A = 'a' - 1;

    public static void main(String[] args) {
        AlfabetGenerator generator = new AlfabetGenerator();
        for (Character character : generator) {
            System.out.println(character);
        }

    }

    @Override
    public Iterator<Character> iterator() {
        return new CharIterator();
    }

    class CharIterator implements Iterator<Character> {
        private char letter = BEFORE_A;

        @Override
        public boolean hasNext() {
            return letter < 'z';
        }

        @Override
        public Character next() {
            if (hasNext()) {
                letter++;
                return letter;
            }else {
                throw new NoSuchElementException();
            }
        }
    }
}
