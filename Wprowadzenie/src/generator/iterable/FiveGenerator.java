package generator.iterable;

import java.util.Iterator;

/**
 * Created by RENT on 2017-07-22.
 */
public class FiveGenerator implements Iterable<Integer> {

    private final int count;

    public FiveGenerator(int count) {
        this.count = count;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new FiveIterator();
    }

    class FiveIterator implements Iterator<Integer> {

        private int counter = 0;

        @Override
        public boolean hasNext() {
            return counter < count;
        }

        @Override
        public Integer next() {
            if (hasNext()) {
                counter++;
                return 5;
            } else {
                return null;
            }
        }
    }

    public static void main(String[] args) {
        FiveGenerator fiveGenerator = new FiveGenerator(6);
        for (Integer number : fiveGenerator) {
            System.out.println(number);
        }
        for (Integer number : fiveGenerator) {
            System.out.println(number);
        }


    }
}

