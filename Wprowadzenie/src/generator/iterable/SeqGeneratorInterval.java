package generator.iterable;

import java.util.Iterator;

/**
 * Created by RENT on 2017-07-22.
 */
public class SeqGeneratorInterval implements Iterable<Integer> {

    private int min;
    private int max;

    public SeqGeneratorInterval(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public static void main(String[] args) {
        SeqGeneratorInterval generatorInterval = new SeqGeneratorInterval(3,6);
        for (Integer integer : generatorInterval) {
            System.out.println(integer);
        }

    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            int number = min;
            @Override
            public boolean hasNext() {
                return number <= max;
            }

            @Override
            public Integer next() {
                return number++;
            }
        };
    }

}
