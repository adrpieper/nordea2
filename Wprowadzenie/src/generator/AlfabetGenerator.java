package generator;

/**
 * Created by RENT on 2017-07-22.
 */
public class AlfabetGenerator {
    public static final char BEFORE_A = 'a' - 1;
    private char letter = BEFORE_A;

    public char generate(){
        if(letter == 'z') {
            letter = BEFORE_A;
        }
        letter++;
        return letter;
    }

    public static void main(String[] args) {
        AlfabetGenerator generator = new AlfabetGenerator();
        for(int i=0;i<30;i++){
            System.out.println(generator.generate());
        }
    }

}
