package generator;

/**
 * Created by RENT on 2017-07-22.
 */
public class MultipleGenerator {

    private final int step;
    private int value = 0;

    public MultipleGenerator(int step) {
        this.step = step;
    }
    public int generate(){
        value += step;
        return value;
    }
    public static void main(String[] args) {
        MultipleGenerator multipleGenerator = new MultipleGenerator(3);
        for(int i=0;i<10;i++){
            System.out.println(multipleGenerator.generate());
        }
    }
}
