package generator.foreach;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by RENT on 2017-07-25.
 */
public class Example {

    public static void main(String[] args) {
        List<Character> characters = Arrays.asList('a','b','c','d');

        System.out.println("For each");
        for (Character character : characters) {
            System.out.println(character);
        }

        System.out.println("For each by iterator");
        Iterator<Character> iterator = characters.iterator();
        while (iterator.hasNext()) {
            Character character = iterator.next();
            System.out.println(character);
        }

    }
}
