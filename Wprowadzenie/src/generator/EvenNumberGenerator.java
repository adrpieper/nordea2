package generator;

/**
 * Created by RENT on 2017-07-22.
 */
public class EvenNumberGenerator {

    private int i;

    public EvenNumberGenerator() {
        i=2;
    }

    public int generate(){
        int value = i;
        i+=2;
        return value;
    }

    public static void main(String[] args) {
        EvenNumberGenerator generator = new EvenNumberGenerator();
        for (int i = 0; i < 10; i++) {
            System.out.println(generator.generate());
        }
    }

}
