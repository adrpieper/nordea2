package generator;

import java.util.Random;

/**
 * Created by RENT on 2017-07-22.
 */
public class SeqRandomNumbers {
    private final Random random = new Random();
    private final int start;
    private final int range;

    public SeqRandomNumbers(int start, int end) {
        this.start = start;
        this.range = end-start+1;
    }

    public int generate(){
        return random.nextInt(range)+start;
    }
    public static void main(String[] args) {
        SeqRandomNumbers generator = new SeqRandomNumbers(4, 6);
        for (int i = 0; i < 3; i++) {
            System.out.println(generator.generate());
        }
    }
}
