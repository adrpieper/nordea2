package generator.inter;

/**
 * Created by RENT on 2017-07-22.
 */
public class AlfabetGenerator implements Generator<Character> {
    public static final char BEFORE_A = 'a' - 1;
    private char letter = BEFORE_A;

    public Character generate(){
        if(letter == 'z') {
            letter = BEFORE_A;
        }
        letter++;
        return letter;
    }

    public static void main(String[] args) {
        AlfabetGenerator generator = new AlfabetGenerator();
        GeneratorMethods.printTenElements(generator);
    }

}
