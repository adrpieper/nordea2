package generator.inter;

import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * Created by RENT on 2017-07-24.
 */
public interface Generator<GeneratedType> {

    GeneratedType generate();

    default void printTenElements() {

        for (int i = 0; i < 10; i++) {
            Object generated = this.generate();
            if (generated != null) {
                System.out.println(generated);
            }else {
                break;
            }
        }
    }

    default void printTenEvenElements() {

        for (int i = 0; i < 10; i++) {
            Object generated = generate();
            if (generated != null) {
                System.out.println(generated);
                generate();
            }else {
                break;
            }
        }
    }

    default void printTenElementsToFile(String fileName) {
        try (PrintStream out = new PrintStream(fileName)){
            for (int i = 0; i < 10; i++) {
                Object generated = generate();
                if (generated != null) {
                    out.println(generated);
                }else {
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
