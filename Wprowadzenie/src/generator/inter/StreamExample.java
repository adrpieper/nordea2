package generator.inter;

import java.util.stream.Stream;

/**
 * Created by RENT on 2017-07-22.
 */
public class StreamExample {

    public static void main(String[] args) {
        SeqGeneratorInterval generator = new SeqGeneratorInterval(2,3);

        Stream.generate(generator::generate)
                .limit(20)
                .forEach(System.out::println);
    }
}
