package generator.inter;

/**
 * Created by RENT on 2017-07-22.
 */
public class SequenceGenerator implements Generator<Integer> {
    private int counter = 1;

    public Integer generate() {
        return counter++;
    }

    public static void main(String[] args) {
        GeneratorMethods.printTenEvenElements(new SequenceGenerator());
        System.out.println("sum of 10 : " + GeneratorMethods.sum(new SequenceGenerator()));
    }
}
