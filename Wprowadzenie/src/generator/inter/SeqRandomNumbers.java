package generator.inter;

import java.util.Random;

/**
 * Created by RENT on 2017-07-22.
 */
public class SeqRandomNumbers implements Generator<Integer> {
    private final Random random = new Random();
    private final int start;
    private final int range;

    public SeqRandomNumbers(int start, int end) {
        this.start = start;
        this.range = end-start+1;
    }

    public Integer generate(){
        return random.nextInt(range)+start;
    }
    public static void main(String[] args) {

        GeneratorMethods.printTenElements(new SeqRandomNumbers(2,5));
    }
}
