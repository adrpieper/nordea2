package generator.inter;

import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * Created by RENT on 2017-07-24.
 */
public class GeneratorMethods {

    public static void printTenElements(Generator generator) {

        for (int i = 0; i < 10; i++) {
            Object generated = generator.generate();
            if (generated != null) {
                System.out.println(generated);
            }else {
                break;
            }
        }
    }


    public static void printTenEvenElements(Generator generator) {

        for (int i = 0; i < 10; i++) {
            Object generated = generator.generate();
            if (generated != null) {
                System.out.println(generated);
                generator.generate();
            }else {
                break;
            }
        }
    }

    public static int sum(Generator<Integer> generator) {
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            Integer generated = generator.generate();
            if (generated != null) {
                sum += generated.intValue();
            }else {
                break;
            }
        }
        return sum;
    }

    public static void printTenElementsToFile(Generator generator, String fileName) {
        try (PrintStream out = new PrintStream(fileName)){
            for (int i = 0; i < 10; i++) {
                Object generated = generator.generate();
                if (generated != null) {
                    out.println(generated);
                }else {
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
