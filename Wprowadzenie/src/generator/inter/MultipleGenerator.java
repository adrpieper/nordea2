package generator.inter;

/**
 * Created by RENT on 2017-07-22.
 */
public class MultipleGenerator implements Generator<Integer> {

    private final int step;
    private int value = 0;

    public MultipleGenerator(int step) {
        this.step = step;
    }
    public Integer generate(){
        value += step;
        return value;
    }
    public static void main(String[] args) {
        GeneratorMethods.printTenElements(new MultipleGenerator(3));
    }
}
