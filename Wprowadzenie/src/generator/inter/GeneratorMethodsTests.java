package generator.inter;

/**
 * Created by RENT on 2017-07-24.
 */
public class GeneratorMethodsTests {

    public static void main(String... args) {
        GeneratorMethods.printTenElements(new Generator() {
            @Override
            public Object generate() {
                return "a";
            }
        });

        int sum = GeneratorMethods.sum(new Generator<Integer>() {
            @Override
            public Integer generate() {
                return 2;
            }
        });

        System.out.println("sum = " + sum);

        GeneratorMethods.printTenEvenElements(new SimpleGenerator());
    }

    static class SimpleGenerator implements Generator<String> {

        String string = "*";

        @Override
        public String generate() {
            return string += "*";
        }
    }

}
