package generator.inter;

/**
 * Created by RENT on 2017-07-22.
 */
public class SeqGeneratorInterval implements Generator<Integer> {

    private int counter;
    private int max;

    public SeqGeneratorInterval(int min, int max) {
        this.counter = min;
        this.max = max;
    }

    public Integer generate(){
        if(counter <max){
            return counter++;
        }
        return null;
    }
    public static void main(String[] args) {
        new SeqGeneratorInterval(2,5).printTenEvenElements();
    }
}
