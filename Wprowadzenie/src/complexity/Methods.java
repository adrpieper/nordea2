package complexity;

import java.util.Random;

/**
 * Created by RENT on 2017-07-20.
 */
public class Methods {

    private static Random random = new Random();

    public static void main(String[] args) {
        System.out.println(sum(1, 2));
        System.out.println(sum(1, 2, 3, 4, 5));
        int[] data = {1, 2, 3, 4, 5};
        System.out.println(sum(data));
        System.out.println(sum(1, 2, 3));
        System.out.println(pow(2, 3));
        System.out.println(pow(2, 4));

        System.out.println("Losowe liczby: ");
        for (int i : randomNumbers(5)) {
            System.out.println(i);
        }

        printRandoms(3);

        int[] sortedArray = {1, 4, 7, 10, 12, 14, 50, 200};
        System.out.println(findIndex(sortedArray, 7));
        ;

        System.out.println(binarySearch(sortedArray, 1));
        System.out.println(binarySearch(sortedArray, 7));
        System.out.println(binarySearch(sortedArray, 50));
        System.out.println(binarySearch(sortedArray, 200));
        System.out.println(binarySearch(sortedArray, -5));
    }

    private static int findIndex(int[] sortedArray, int value) {
        for (int i = 0; i < sortedArray.length; i++) {
            if (sortedArray[i] == value) {
                return i;
            }
        }

        return -1;
    }


    private static int binarySearch(int[] sortedArray, int value) {
        int low = 0;
        int high = sortedArray.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            int midValue = sortedArray[mid];
            if (value < midValue) {
                high = mid - 1;
            } else if (value > midValue) {
                low = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }


    public static int sum(int a, int b) { // O(1)
        return a + b;
    }

    public static int sum(int a, int b, int c, int d, int e) { //O(1)
        return a + b + c + d + e;
    }

    //                     int... - varargs - dowolna ilość parametrów
    public static int sum(int... data) { //O(n)
        int r = 0;

        for (int i : data) {
            r += i;
        }
        return r;
    }

    public static int pow(int a, int b) {
        int result = 1;
        for (int i = 0; i < b; i++) {
            result *= a;
        }
        return result;
    }

    public static int[] empty(int size) { // O(n)
        return new int[size];
    }

    public static int[] randomNumbers(int size) { // O(n)
        int[] ints = new int[size];
        for (int i = 0; i < ints.length; i++) {
            ints[i] = random.nextInt(10);
        }
        return ints;
    }

    public static int[][] randomNumbers2(int size) { //O(n^2)
        int[][] result = new int[size][];
        for (int i = 0; i < result.length; i++) {
            result[i] = randomNumbers(size);
        }
        return result;
    }

    static int[][] randomNumbers3(int size) { //O(n^2)
        int[][] result = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                result[i][j] = random.nextInt(10);
            }
        }
        return result;
    }

    private static void printRandoms(int size) {
        int[][] randomNumb = randomNumbers2(size);
        System.out.println("_____________________________________");
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(randomNumb[i][j] + " || ");
            }
            System.out.println();
        }
        System.out.println("_____________________________________");
    }
}
