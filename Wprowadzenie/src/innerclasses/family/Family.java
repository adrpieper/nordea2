package innerclasses.family;

/**
 * Created by RENT on 2017-07-25.
 */
public class Family {
    private final Mother mother;
    private final Father father;

    public Family(String motherName, String fatherName) {
        mother = new Mother(motherName);
        father = new Father(fatherName);
    }

    public static void main(String[] args) {
        Family family = new Family("Anna", "Jan");
        family.introduce();
    }

    private void introduce() {
        mother.introduce();
        father.introduce();
    }

    class Mother {
        private String name;

        Mother(String name) {
            this.name = name;
        }

        private void introduce(){
            System.out.println("Nazywam się " + name + ", mój mąż to " + father.name);
        }
    }

    class Father {
        private String name;
        public Father(String name) {
            this.name = name;
        }
        private void introduce(){
            System.out.println("Nazywam się " + name + ", moja żona to " + mother.name);
        }
    }
}