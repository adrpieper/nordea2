package innerclasses.village;

/**
 * Created by RENT on 2017-07-25.
 */
public class Village {
    private int food = 0;
    private Farmer farmer1 = new Farmer();
    private Farmer farmer2 = new Farmer();

    public static void main(String[] args) {
        Village village = new Village();
        village.work();
        System.out.println(village.getFood());
    }

    public void work() {
        farmer1.work();
        farmer2.work();
    }

    public int getFood() {
        return food;
    }

    private class Farmer {

        public void work() {
            food += 10;
        }
    }
}
