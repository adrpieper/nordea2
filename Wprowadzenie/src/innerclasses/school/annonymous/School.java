package innerclasses.school.annonymous;

/**
 * Created by RENT on 2017-07-24.
 */
public class School {
    private String name;
    private String patron;

    public School(String name, String patron) {
        this.name = name;
        this.patron = patron;
    }

    public Pupil newPupil(String name) {
        // Anonimowa niestatyczna klasa wewnętrzna
        Pupil pupil = new Pupil() {
            @Override
            public void introduce() {
                System.out.println("Nazywam się " + name + ". Uczęszczam do szkoły "
                        + School.this.name + " imienia " + patron);

            }
        };
        return pupil;
    }

    public static void main(String[] args) {
        School school = new School("I LO", "Jan III Sobieski");
        // Anonimowa wewnętrzna statyczna klasa
        Pupil staticPupil = new Pupil() {
            @Override
            public void introduce() {
                System.out.println("Nazywam się Jan. Uczęszczam do szkoły "
                        + school.name + " imienia " + school.patron);
            }
        };
        Pupil nostaticPupil = school.newPupil("Adam");
        staticPupil.introduce();
        nostaticPupil.introduce();

        // Nie można tworzyć instacji klas wewnętrzych w kontekście statycznym
        // new Pupil("Adam");
    }

    interface Pupil {
        void introduce();
    }
}
