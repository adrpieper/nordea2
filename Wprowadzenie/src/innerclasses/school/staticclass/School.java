package innerclasses.school.staticclass;

/**
 * Created by RENT on 2017-07-24.
 */
public class School {
    private String name;
    private String patron;

    public School(String name, String patron) {
        this.name = name;
        this.patron = patron;
    }

    public static void main(String[] args) {
        School school = new School("I LO", "Sobieski");
        // Szkołę trzeba przekazać jako parametr konstruktora,
        // bo Pupil jest elementem statycznym klasy School
        Pupil pupil = new Pupil("Jan", school);
        pupil.introduce();

    }

    static class Pupil {
        private String name;
        private School school;

        Pupil(String name, School school) {
            this.name = name;
            this.school = school;
        }

        public void introduce() {
            System.out.println("Nazywam się " + name+ ". Uczęszczam do szkoły "
                    + school.name + " imienia "+ school.patron);
        }
    }
}
