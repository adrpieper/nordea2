package files;

import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * Created by RENT on 2017-07-24.
 */
public class SimpleExample {

    public static void main(String[] args) {


        try (PrintStream out = new PrintStream("HelloWorld.txt")){
            out.println("Hello World!!!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        PrintStream out = null;
        try {
            out = new PrintStream("HelloWorld.txt");
            out.println("Hello World!!!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            if (out != null) {
                out.close();
            }
        }

        try (MyClass myClass = new MyClass()) {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class MyClass implements AutoCloseable {

        @Override
        public void close() throws Exception {
            System.out.println("Zamykam");
        }
    }
}
