package loop;

import java.util.Scanner;

/**
 * Created by RENT on 2017-07-08.
 */
public class HelloWorldFewTimes {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile razy mam wypisać Hello World: ");
        int helloWorldCount = scanner.nextInt();

        for (int i=0;i<helloWorldCount;i++){
            System.out.println("Hello world");
        }
    }
}
