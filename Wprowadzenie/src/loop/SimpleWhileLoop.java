package loop;

/**
 * Created by RENT on 2017-07-08.
 */
public class SimpleWhileLoop {

    public static void main(String[] args) {

        int i = 1;
        while(i<=10){
            System.out.println(i);
            i++;
        }
        while(i<=20){
            if(i%2==0) {
                System.out.println(i);
            }
            i++;
        }
        while(i<=100){
            if(i%3 ==0 || i%5 ==0){
                System.out.println(i);
            }
            i++;
        }
        char c='a';
        while(c<='z'){
            System.out.println(c);
            c++;
        }
    }
}
