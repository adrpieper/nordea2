package loop;

import java.util.Scanner;

/**
 * Created by RENT on 2017-07-08.
 */
public class DoWhile {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i;
        do {
            System.out.println("Podaj liczębe większa od 0 to coś napiszę: ");
            i = scanner.nextInt();
            if (i > 0) {
                System.out.println("Hello world");
            }
        } while (i > 0);
        System.out.println("Wprowadzies liczbe 0, koniec programu :(");
    }
}
