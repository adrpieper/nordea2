package loop;

/**
 * Created by RENT on 2017-07-08.
 */
public class SimpleForLoop {

    public static void main(String[] args) {

        System.out.println("1 ... 10");
        for (int i=1; i<=10; i++){
            System.out.println(i);
        }

        System.out.println("2 4 ... 20");
        for(int i=2;i<=20;i+=2){
            System.out.println(i);
        }

        for (int i = 1; i<=20; i++) {
            if (i%2==0) {
                System.out.println(i);
            }
        }


        System.out.println("Podzielne przez 3 lub 5:");
        for(int i=3;i<=100;i++) {
            if (i%3==0 || i%5==0) {
                System.out.println(i);
            }
        }

        System.out.println("Podzielne przez 3 albo 5:");
        for(int i=3;i<=100;i++) {
            if ((i%3==0) != (i%5==0)) {
                System.out.println(i);
            }
        }

        System.out.println("Od a do z:");
        for(int i=(int)'a';i<=(int)'z';i++){
            System.out.println((char)i);
        }

        for(char c='a';c<='z';c++){
            System.out.println(c);
        }
    }
}
