package loop.calculation;

/**
 * Created by Adrian on 2017-07-10.
 */
public class StringsLengthSumAndMean {

    public static void main(String[] args) {
        String[] words = "Ala;ma;kota".split(";");
        int suma = 0;
        for (String word : words) {
            suma += word.length();
        }
        System.out.println("Suma liter w wyrazach: " + suma);
        System.out.println("Średnia długość słowa: " + (double) suma/words.length);

    }
}
