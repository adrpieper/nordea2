package loop.calculation;

/**
 * Created by Adrian on 2017-07-10.
 */
public class SumOfGeometricalSeries {

    public static void main(String[] args) {
        int a0 = 2;
        int q = 3;
        int n = 3;

        int suma = 0; // 2 + 6 + 18

        int a = a0;
        for (int i = 0 ; i < n ; i++) {
            suma += a;
            a *= q;
        }
        System.out.println("Suma ciąg : " + suma);
    }
}
