package loop.calculation;

/**
 * Created by Adrian on 2017-07-10.
 */
public class SumOfAritmeticalSeries {

    public static void main(String[] args) {
        int a0 = 1;
        int r = 2;
        int n = 4;

        int suma = 0; // 1 + 3 + 5 + 7

        for (int i = 0; i < n; i++) {
            suma = suma + (a0 + r * i);
        }
        System.out.println(suma);

        int suma1 = 0; // 1 + 3 + 5 + 7
        int a = a0;
        for (int i = 0; i < n; i++) {
            suma1 +=  a;
            a += r;
        }
        System.out.println(suma1);
    }

}
