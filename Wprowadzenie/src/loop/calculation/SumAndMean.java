package loop.calculation;

/**
 * Created by Adrian on 2017-07-10.
 */
public class SumAndMean {

    public static void main(String[] args) {
        int[] data = {1, 3, 2, 3};

        int suma = countSum(data);
        System.out.println("Suma liczb w tablicy to: " + suma);
        System.out.println("Średnia w tabeli to: " + (double) suma / data.length);
        System.out.println("Średnia w tabeli to: " + 1.0 * suma / data.length);
        double sumaAsDouble = suma;
        System.out.println("Średnia w tabeli to: " + sumaAsDouble / data.length);
    }

    private static int countSum(int[] data) {
        int suma = 0;
        for (int number : data) {
            suma += number;
        }
        return suma;
    }
}
