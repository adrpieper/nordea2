package loop.calculation;

/**
 * Created by Adrian on 2017-07-10.
 */
public class Multiplication {
    public static void main(String[] args) {
        int[] data = {1, 3, 2, 3};
        int iloczyn = 1;
        for (int number : data) {
            iloczyn *= number;
        }
        System.out.println("Iloczyn liczb w tablicy to: " + iloczyn);
    }
}
