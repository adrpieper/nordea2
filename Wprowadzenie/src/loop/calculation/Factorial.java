package loop.calculation;

/**
 * Created by Adrian on 2017-07-10.
 */
public class Factorial {

    public static void main(String[] args) {

        System.out.println("4! = " + factorial(4));
        System.out.println("5! = " + factorial(5));
    }

    private static int factorial(int n) {
        int silnia = 1;

        for (int i = 2; i <= n; i++) {
            silnia = silnia * i;
        }
        return silnia;
    }
}
