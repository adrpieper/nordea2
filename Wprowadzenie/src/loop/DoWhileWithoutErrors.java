package loop;

import java.util.Scanner;

/**
 * Created by RENT on 2017-07-08.
 */
public class DoWhileWithoutErrors {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj liczbę :");
        int liczba;
        do {
            if(sc.hasNextInt() ) {
                liczba = sc.nextInt();
                if (liczba > 0) {
                    System.out.println("Hello World");
                }
            } else {
                System.out.println("Złe dane");
                break;
            }
        }while(liczba > 0);
        System.out.println("Koniec zabawy");
    }
}
