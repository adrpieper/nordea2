package collection.set;

/**
 * Created by RENT on 2017-07-17.
 */
public class IntPair {
    private int a;
    private int b;

    public IntPair(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IntPair pair = (IntPair) o;

        return a == pair.a && b == pair.b;
    }

    @Override
    public int hashCode() {
        return 31 * a + b;
    }
}
