package collection.set;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by RENT on 2017-07-17.
 */
public class Zad1 {

    public static void main(String[] args) {
        int[] data = {10,12,10,3,4,12,12,300,12,40,55};
        Set<Integer> set = new TreeSet<>();
        for (int i : data) {
            set.add(i);
        }

        System.out.println("Size of set : " + set.size());

        for (int number : set) {
            System.out.println(number);
        }

        set.remove(10);
        set.remove(12);

        System.out.println("Size of set : " + set.size());

        for (int number : set) {
            System.out.println(number);
        }

    }
}
