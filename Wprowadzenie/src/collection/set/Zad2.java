package collection.set;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by RENT on 2017-07-17.
 */
public class Zad2 {

    public static void main(String[] args) {

        System.out.println(containDuplicates("sentence"));
        System.out.println(containDuplicates("abc"));
    }
    public static boolean containDuplicates(String text){
        Set<Character> set = new HashSet<>();
        for (char c: text.toCharArray()){
            set.add(c);
        }
        return set.size()!=text.length();
    }

}
