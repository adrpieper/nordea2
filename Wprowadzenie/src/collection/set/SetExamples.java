package collection.set;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by RENT on 2017-07-17.
 */
public class SetExamples {

    public static void main(String[] args) {
        Set<Character> characters = new HashSet<>();
        characters.add('a');
        characters.add('z');
        characters.add('t');
        characters.remove('z');
        System.out.println(characters.iterator().next());
        System.out.println("contains a : " + characters.contains('a'));
        System.out.println("contains z : " + characters.contains('z'));

        System.out.println("For each");
        for (Character c : characters) {
            System.out.println(c);
        }
        System.out.println("rozmiar : " + characters.size());
        characters.clear();

        System.out.println("Po wywołaniu metody clear");
        System.out.println("rozmiar : " + characters.size());

    }
}
