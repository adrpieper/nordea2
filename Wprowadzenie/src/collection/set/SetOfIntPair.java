package collection.set;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by RENT on 2017-07-17.
 */
public class SetOfIntPair {


    public static void main(String[] args) {
        Set<IntPair> set = new HashSet<>();
        set.add(new IntPair(1,2));
        set.add(new IntPair(2,1));
        set.add(new IntPair(1,1));
        set.add(new IntPair(1,2));

        for (IntPair pair : set) {
            System.out.println("{" + pair.getA() + "," + pair.getB() + "}");
        }
        System.out.println(set);


        IntPair a = new IntPair(1,2);
        IntPair b = new IntPair(1,2);
        System.out.println(a.equals(b));
        System.out.println(a.equals(null));
        System.out.println(a.equals("abd"));

    }
}
