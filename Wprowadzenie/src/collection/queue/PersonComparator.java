package collection.queue;

import java.util.Comparator;

/**
 * Created by RENT on 2017-07-18.
 */
public class PersonComparator implements Comparator<Person> {
    @Override
    public int compare(Person o1, Person o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
