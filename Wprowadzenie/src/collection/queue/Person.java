package collection.queue;

/**
 * Created by RENT on 2017-07-18.
 */
public class Person implements Comparable<Person> {

    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public String getName() {
        return name;
    }
    public int getAge() {
        return age;
    }

    @Override
    public int compareTo(Person other) {
//        if (this.age == other.age)
//            return 0;
//        else if (this.age > other.age)
//            return -1;
//        else
//            return 1;
        return other.age - this.age ;
    }
}
