package collection.queue;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by RENT on 2017-07-18.
 */
public class PersonQueue {

    public static void main(String[] args) {
        Queue<Person> people = new PriorityQueue<>((o1,o2) -> o1.getName().compareTo(o2.getName()));

        people.offer(new Person("Jan", 11));
        people.offer(new Person("Anna", 23));
        people.offer(new Person("Kasia", 21));
        people.offer(new Person("Janusz", 15));

        while (people.size() > 0) {
            System.out.println(people.poll().getName());
        }
    }
}
