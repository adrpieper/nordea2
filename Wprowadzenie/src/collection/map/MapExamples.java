package collection.map;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by RENT on 2017-07-17.
 */
public class MapExamples {

    public static void main(String[] args) {

        Map<Character, Integer> map = new HashMap<>();

        map.put('a', 1);
        map.put('b', 2);
        map.put('ź', 5);

        System.out.println(map);

        System.out.println("Size of map : " + map.size());
        System.out.println("Contains Key a : " + map.containsKey('a'));
        System.out.println("Contains Key c : " + map.containsKey('c'));
        System.out.println("Contains Value 2 : " + map.containsValue(2));
        System.out.println("Contains Value 3 : " + map.containsValue(3));

        map.remove('a', 3);
        System.out.println(map);
        map.remove('a', 1);
        map.remove('b');
        System.out.println(map);
        map.put('ź', null);
        System.out.println(map);

        map.put('a', 1);
        map.put('b', 2);

        System.out.println(map.keySet());
        System.out.println(map.values());
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "->" + entry.getValue());
        }

        System.out.println("Value of a : " + map.get('a'));
        System.out.println("Value of r : " + map.get('r'));
        System.out.println("Value of r : " + map.getOrDefault('a',9));
        System.out.println("Value of r : " + map.getOrDefault('r',9));


    }
}
