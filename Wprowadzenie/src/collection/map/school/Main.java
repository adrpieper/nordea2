package collection.map.school;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by RENT on 2017-07-18.
 */
public class Main {

    public static void main(String[] args) {
        Map<Long, Student> map = new HashMap<>();
        Student[] students = {
                new Student(100100, "Mati", "Mati"),
                new Student(100200, "Danny", "Danny"),
                new Student(100300, "Caro", "Caro"),
                new Student(100400, "Adi", "Adi"),
                new Student(100400, "Adi", "Tadi")
        };

        for (Student student : students) {
            map.put(student.getIndex(), student);
        }


        System.out.println("Czy w systemie jest student o indeksie 100200: " + map.containsKey(100200L));
        Student student100200 = map.get(100200L);
        System.out.println("Student o indeksie 100200: " + student100200.getName() + " " + student100200.getSurname());
        Student student100400 = map.get(100400L);
        System.out.println("Student o indeksie 100400: " + student100400.getName() + " " + student100400.getSurname());
        System.out.println("W uczelni jest: " + map.size()+" studentów");
    }
}
