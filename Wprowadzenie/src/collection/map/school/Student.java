package collection.map.school;

/**
 * Created by RENT on 2017-07-18.
 */
public class Student {

    private long index;
    private String name;
    private String surname;

    public Student(long index, String name, String surname) {
        this.index = index;
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public long getIndex() {
        return index;
    }
}
