package collection.map.school;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by RENT on 2017-07-18.
 */
public class University {

    private Map<Long, Student> map = new HashMap<>();

    public void addStudent(long indexNumber, String name, String surname) {
        Student student = new Student(indexNumber, name, surname);
        map.put(indexNumber, student);
    }

    public int getStudentCount() {
        return map.size();
    }

    public boolean containsStudent(long index){
        return map.containsKey(index);
    }

    public void printAllStudents(){
        for (Student student : map.values()) {
            System.out.println(student.getName() + " " + student.getSurname() + " nr indexu: " + student.getIndex());
        }
    }

    public Student getStudent(long indexNumber){
        return map.get(indexNumber);
    }
}
