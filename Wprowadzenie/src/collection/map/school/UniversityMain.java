package collection.map.school;

/**
 * Created by RENT on 2017-07-18.
 */
public class UniversityMain {

    public static void main(String[] args) {
        University university = new University();
        addStudents(university);
        checkStudents(university);
        printStudent(university, 100400);
        university.printAllStudents();
    }

    private static void printStudent(University university, int indexNumber) {
        Student student100400 = university.getStudent(indexNumber);
        System.out.println(student100400.getName() + " " + student100400.getSurname());
    }

    private static void checkStudents(University university) {
        System.out.println(university.containsStudent(100400));
        System.out.println(university.containsStudent(100500));
    }

    private static void addStudents(University university) {
        university.addStudent(100100, "Mati", "Mati");
        university.addStudent(100100, "Mati", "Mati");
        university.addStudent(100200, "Danny", "Danny");
        university.addStudent(100300, "Caro", "Caro");
        university.addStudent(100400, "Adi", "Adi");
        university.addStudent(100400, "Adi", "Tadi");
    }
}
