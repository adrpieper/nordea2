package collection.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-07-15.
 */
public class ListCalculations {

    public static void main(String[] args) {
        List<Double> numbers = new ArrayList<>();
        numbers.add(1.0);
        numbers.add(2.0);
        numbers.add(3.0);
        numbers.add(4.0);
        System.out.println("suma : " + sum(numbers));
        System.out.println("iloczyn : " + product(numbers));
        System.out.println("średnia : " + mean(numbers));
    }

    public static double sum(List<Double> numbers) {
        double sum = 0.0;
        for (double value : numbers) {
            sum+=value;
        }
        return sum;
    }

    public static double product(List<Double> numbers) {
        double product = 1.0;
        for (double value : numbers) {
            product*=value;
        }
        return product;
    }

    public static double mean(List<Double> numbers) {
        return sum(numbers) / numbers.size();
    }

}
