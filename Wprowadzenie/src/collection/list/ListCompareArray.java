package collection.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-07-17.
 */
public class ListCompareArray {

    public static void main(String[] args) {
        double[] doubles = {4,2,2,1,5,29,3,8};
        List<Double> doubleList = new ArrayList<>();
        doubleList.add(1.0);
        doubleList.add(2.0);
        doubleList.add(4.0);
        doubleList.add(2.0);
        doubleList.add(5.0);
        doubleList.add(12.0);
        doubleList.add(3.0);
        doubleList.add(2.0);

        System.out.println("Tablice mają " +
                getCommonElementsCount(doubles, doubleList)+
                " pozycji(e) o identycznej wartości.");
    }

    public static int getCommonElementsCount(double[] doubles, List<Double> doubleList) {
        int count = 0;
        int elements = Integer.min(doubles.length, doubleList.size());
        for (int i = 0; i < elements; i++) {
            if (doubles[i] == doubleList.get(i)){
                count++;
            }
        }
        return count;
    }

}
