package collection.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-07-15.
 */
public class ListExample {

    public static void main(String[] args) {

        List<Integer> list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        for (Integer integer : list) {
            System.out.println(integer);
        }

        List<String> strings = new ArrayList<>();


        strings.add("d");
        strings.add("a");
        strings.add("b");
        strings.add("c");
        strings.add("d");

        System.out.println("Znaki : ");
        for (String string : strings) {
            System.out.println(string);
        }

        strings.remove(1);

        System.out.println("Znaki : ");
        for (String string : strings) {
            System.out.println(string);
        }

        strings.remove("d");

        System.out.println("Znaki : ");
        for (String string : strings) {
            System.out.println(string);
        }

        System.out.println("Size : " + strings.size());

        System.out.println("Index of c : " + strings.indexOf("c"));
        System.out.println("Contains a : " + strings.contains("a"));
        System.out.println("Contains b : " + strings.contains("b"));


        System.out.println("Element 1. : " + strings.get(1));
    }
}
