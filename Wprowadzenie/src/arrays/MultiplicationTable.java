package arrays;

/**
 * Created by Adrian on 2017-07-10.
 */
public class MultiplicationTable {

    public static void main(String[] args) {
        int[][] tabliczkaMnozenia = new int[4][4];
        for (int i = 0; i < tabliczkaMnozenia.length; i++) {
            for (int j = 0; j < tabliczkaMnozenia[i].length; j++) {
                tabliczkaMnozenia[i][j] = (i + 1) * (j + 1);
            }
        }
        for (int[] wiersz : tabliczkaMnozenia) {
            for (int wartosc : wiersz) {
                System.out.print(' ');
                if (wartosc < 10) {
                    System.out.print(' ');
                }
                System.out.print(wartosc);
            }
            System.out.println();
        }
    }
}
