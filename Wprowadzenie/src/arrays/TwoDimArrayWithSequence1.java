package arrays;

/**
 * Created by Adrian on 2017-07-10.
 */
public class TwoDimArrayWithSequence1 {

    public static void main(String[] args) {
        int[][] sequence = new int[4][4];
        for (int i = 0; i < sequence.length; i++) {
            for (int j = 0; j < sequence[i].length; j++) {
                sequence[i][j] = i*sequence[i].length+j+1;
            }
        }
        for (int i = 0; i < sequence.length; i++) {
            for (int j = 0; j < sequence[i].length; j++) {
                System.out.print(sequence[i][j]+" ");
            }
            System.out.println();
        }
    }
}
