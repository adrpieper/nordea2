package arrays;

/**
 * Created by RENT on 2017-07-08.
 */
public class NamesArray {

    public static void main(String[] args) {

        String[] names = {"Ania", "Kasia", "Basia", "Zosia"};

        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i]);
        }

        for (String i : names) {
            System.out.println(i);
        }

        System.out.println("Co drugie imię: ");
        for (int i = 0; i < names.length; i = i + 2) {
            System.out.println(names[i]);
        }

        System.out.println("Imiona zaczynajace sie na A: ");
        for (int i = 0; i < names.length; i++) {
            String imie = names[i];
            char imieTab[];
            imieTab = imie.toCharArray();
            if (imieTab[0] == 'A') {
                System.out.println(names[i]);
            }
        }

        System.out.println("Imiona zaczynajace sie na A: ");
        for (String imie : names) {
            if (imie.toUpperCase().charAt(0) == 'A') {
                System.out.println(imie);
            }
        }

        System.out.println("Imiona zaczynajace sie na A: ");
        for (String imie : names) {
            if (imie.toUpperCase().startsWith("A")) {
                System.out.println(imie);
            }
        }
    }
}
