package arrays;

/**
 * Created by Adrian on 2017-07-10.
 */
public class TwoDimArrayWithSequence {

    public static void main(String[] args) {
        int sequence[][] = new int[4][4];
        int k = 1;
        for (int i = 0; i < sequence.length; i++) {
            for (int j = 0; j < sequence[i].length; j++) {
                sequence[i][j] = k++;
            }
        }
        for (int[] row : sequence) {
            for (int number : row) {
                System.out.print(number + " ");
            }
            System.out.println("");
        }
    }
}
