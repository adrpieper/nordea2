package arrays;

/**
 * Created by RENT on 2017-07-08.
 */
public class StringsArrayLoop {

    public static void main(String[] args) {
        //String[] slowa = {"aa","bb", "cc", "dd"};
        String[] slowa = "aa,bb,cc,dd".split(",");

        for (int i = 0; i<slowa.length; i++) {
            System.out.println(slowa[i]);
        }

        for (String number : slowa) {
            System.out.println(number);
        }

        for (int i = slowa.length-1; i>=0; i--){
            System.out.println(slowa[i]);
        }
    }
}
