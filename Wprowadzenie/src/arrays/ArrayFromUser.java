package arrays;

import java.util.Scanner;

/**
 * Created by RENT on 2017-07-08.
 */
public class ArrayFromUser {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wielkość tabeli liczb całkowitych");
        int argumentsNumber = scanner.nextInt();
        int [] userTab = new int[argumentsNumber];

        for (int i = 0; i < argumentsNumber; i++) {
            System.out.println("Podaj element " + (i+1) + ".");
            userTab[i] = scanner.nextInt();
        }

        System.out.println("Twoja tablica :");
        for (int number : userTab) {
            System.out.println(number);
        }
    }
}
