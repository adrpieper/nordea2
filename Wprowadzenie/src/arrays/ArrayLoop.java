package arrays;

/**
 * Created by RENT on 2017-07-08.
 */
public class ArrayLoop {

    public static void main(String[] args) {
        int[] numbers = {1,3,5,10};
        System.out.println(numbers[0]);
        System.out.println(numbers[1]);
        System.out.println(numbers[2]);
        System.out.println(numbers[3]);

        for(int i = 0;i<numbers.length;i++)
            System.out.println(numbers[i]);

        for(int i = numbers.length-1;i>=0;i--){
            System.out.println(numbers[i]);
        }

        for(int number : numbers){
            System.out.println(number);
        }
    }
}
