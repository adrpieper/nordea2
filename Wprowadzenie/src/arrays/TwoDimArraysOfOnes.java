package arrays;

/**
 * Created by RENT on 2017-07-08.
 */
public class TwoDimArraysOfOnes {

    public static void main(String[] args) {
        int[][] numbers = new int[3][4];
        //numbers[0] = new int[4];
        //numbers[1] = new int[4];
        //numbers[2] = new int[4];

        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < numbers[i].length; j++) {
                numbers[i][j] = 1;
            }
        }

        for (int i = 0; i < numbers.length; i++) {
            int[] row = numbers[i];
            System.out.print("Rząd " + (i + 1) + ": ");
            for (int j : row) {
                System.out.print(j);
                System.out.print(' ');
            }
            System.out.println();
        }
    }
}
