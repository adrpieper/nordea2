package arrays;

/**
 * Created by RENT on 2017-07-08.
 */
public class ArraysExample {

    public static void main(String[] args) {

        int[] numbers = new int[10];
        System.out.println(numbers[5]);
        int[] ones = {1,1,1,1};
        int[] numbers3 = {1,2,3};
        System.out.println(numbers3[0]);
        System.out.println(numbers3[2]);
        String[] words = {"Stół", "Krzesło"};
        System.out.println(words[0]);
        int[][] arrays = {numbers, ones, {5}, null};
        System.out.println(arrays[2][0]);
        System.out.println(arrays[3]);
        //System.out.println(arrays[3][0]); NullPointerException
        int[][] arrays2 = new int[4][];
        System.out.println(arrays2[2]);
        String[] strings = new String[6];
        System.out.println(strings[2]);
        int[][] numbers2 = new int[3][4];
        System.out.println(numbers2[0]);
        System.out.println(numbers2[0][0]);
        System.out.println(numbers2[1][0]);
        System.out.println(numbers2[1][2]);
        // System.out.println(numbers2[3][2]); NullPointerException

        char[] chars = new char[4];
        chars[0] = 'a';
        chars[1] = 'b';
        chars[2] = 'c';
        System.out.println(chars[0]);
        System.out.println(chars[1]);
        System.out.println(chars[2]);
        System.out.println(chars[3]);

        int[] data = {21,32,100};
        for(int i = 0; i < data.length; i++) {
            System.out.println(data[i]);
        }

        for (int number : data) {
            System.out.println(number);
        }
    }
}
