package datatypes;

/**
 * Created by Adrian on 2017-07-06.
 */
public class DeklarowanieWypisywanie {

    /*
    Napisz program, który deklaruje,
    inicjuje (nadaje wartość)
    i wypisuje na ekran zmienne każdego znanego przez ciebie typu.

     */
    public static void main(String[] args) {
        byte bajt = 123;
        short s = 1000;
        int i = 10;
        long l = 234567890;
        char c = 's';
        float f = 234.34f;
        double d = 2322.345678;
        boolean b = true;
        String n = "napis";
        System.out.println("byte: " + bajt);
        System.out.println("short: " + s);
        System.out.println("int: " + i);
        System.out.println("long: " + l);
        System.out.println("char: " + c);
        System.out.println("float: " + f);
        System.out.println("double: " + d);
        System.out.println("boolean: " + b);
        System.out.println("String: " + n);

    }
}
