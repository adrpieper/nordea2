package datatypes;

/**
 * Created by Adrian on 2017-07-06.
 */
public class InstrukcjeWarunkowe {

    public static void main(String[] args) {

        if(2>3){
            System.out.println(":)");
        }else{
            System.out.println(":(");
        }

        if(4<5){
            System.out.println(":)");
        }else{
            System.out.println(":(");
        }

        if((2-2) == 0){
            System.out.println(":)");
        }else{
            System.out.println(":(");
        }

        if(true){
            System.out.println(":)");
        }else{
            System.out.println(":(");
        }

        if( 9%2 == 0 ){
            System.out.println(":)");
        }else{
            System.out.println(":(");
        }

        if( 9%3 == 0 ){
            System.out.println(":)");
        }else{
            System.out.println(":(");
        }
    }
}
