package datatypes;

import java.util.Scanner;

/**
 * Created by Adrian on 2017-07-06.
 */
public class Kalkulator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę");
        int a = scanner.nextInt();
        System.out.println("Podaj drugą liczbę");
        int b = scanner.nextInt();
        System.out.println("Podaj działanie wpisując +, -, *, /");
        String c = scanner.next();
        switch (c) {
            case "+":
                System.out.println(a + b);
                break;
            case "-":
                System.out.println(a - b);
                break;
            case "*":
                System.out.println(a * b);
                break;
            case "/":
                System.out.println((double) a / (double) b);
                System.out.println(a / b + " reszty " + a % b);
                System.out.println(a / b + " " + a % b + "/" + b);
                break;
            default:
                System.out.println("Brak operacji");
                break;
        }
    }
}
