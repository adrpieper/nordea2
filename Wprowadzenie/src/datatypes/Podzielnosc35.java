package datatypes;


import java.util.Scanner;

/**
 * Created by Adrian on 2017-07-06.
 */
public class Podzielnosc35 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę");
        int i = scanner.nextInt();
        if (i % 3 == 0) {
            System.out.println("Liczba podzielna przez 3 yeah");
        } else if (i % 5 == 0) {
            System.out.println("Liczba podzielna przez 5 yeah");
        } else {
            System.out.println("Liczba nie jest podzielna ani przez 3 ani przez 5");
        }

    }
}
