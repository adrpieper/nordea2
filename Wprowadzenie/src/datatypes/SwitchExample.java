package datatypes;

/**
 * Created by Adrian on 2017-07-06.
 */
public class SwitchExample {

    public static void main(String[] args) {

        int i = 2;

        /*
        Jeżeli nie ma instrucji break, to wykonują się wszystkie
        instrukcje od poprawnej wartości w dół.
         */
        switch (i) {
            case 1:
                System.out.println("1");
                break;
            case 2:
                System.out.println("2");
                break;
            case 3:
                System.out.println("3");
                break;
            default:
                System.out.println("?");
                break;
        }
    }

}
