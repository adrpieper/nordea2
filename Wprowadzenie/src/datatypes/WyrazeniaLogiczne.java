package datatypes;

/**
 * Created by Adrian on 2017-07-06.
 */
public class WyrazeniaLogiczne {

    public static void main(String[] args) {
        System.out.println(false ==false); //true
        System.out.println(false!=true); //true
        System.out.println(!true); //false
        System.out.println(2>4); //false
        System.out.println(3<5); //true
        System.out.println(3==3&&3==4); //false
        System.out.println(3!=5||3==5); // true
        System.out.println((2+4)>(1+3)); //true
        System.out.println("cos".equals("cos")); //true
        System.out.println("cos"=="cos"); //true

        String cos = new String("cos");
        System.out.println(cos);
        System.out.println(cos.equals("cos")); //true
        System.out.println(cos =="cos"); //false
    }
}
