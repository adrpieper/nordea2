package datatypes;

/**
 * Created by Adrian on 2017-07-06.
 */
public class Dzialania {

    public static void main(String[] args) {
        int suma23 = 2 + 3;
        System.out.println(suma23);
        int roznica42 = 4 - 2;
        System.out.println(roznica42);
        int iloraz52 = 5 / 2;
        System.out.println(iloraz52);
        double iloraz502 = 5.0 / 2;
        System.out.println(iloraz502);
        double iloraz520 = 5 / 2.0;
        System.out.println(iloraz520);
        double iloraz5020 = 5.0 / 2.0;
        System.out.println(iloraz5020);
        long roznica100L10 = 100L - 10;
        System.out.println(roznica100L10);
        float roznica2f3 = 2f - 3;
        System.out.println(roznica2f3);
        float iloraz5f2 = 5f / 2;
        System.out.println(iloraz5f2);
        double iloraz5d2 = 5d / 2;
        System.out.println(iloraz5d2);
        int sumaA2 = 'A' + 2;
        System.out.println(sumaA2);
        int sumaa2 = 'a' + 2;
        System.out.println(sumaa2);
        String konkatenacjaa2 = "a" + 2;
        System.out.println(konkatenacjaa2);
        String konkatenacjaab = "a" + "b";
        System.out.println(konkatenacjaab);
        int sumaab = 'a' + 'b';
        System.out.println(sumaab);
        String konkatenacjaab2 = "a" + 'b';
        System.out.println(konkatenacjaab2);
        String konkatenacjaab3 = "a" + 'b' + 3;
        System.out.println(konkatenacjaab3);
        String sumaIkonkatenacjab3a = 'b' + 3 + "a";
        System.out.println(sumaIkonkatenacjab3a);
        int modulo94 = 9 % 4;
        System.out.println(modulo94);

        System.out.println(2);
        System.out.println(2f);
    }
}
