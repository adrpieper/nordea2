package oop.mutability.string;

/**
 * Created by RENT on 2017-07-22.
 */
public class StringExample {

    public static void main(String[] args) {
        String a = "a";
        String b = "a";
        System.out.println(a == b); // false TRUE|FALSE
        System.out.println(a.equals(b)); // true TRUE
        b = "bb";
        a = b;
        System.out.println(a == b); // true TRUE
        System.out.println(a.equals(b)); // true TRUE
        b = "bbb";
        System.out.println(a == b); // false | true FALSE
        System.out.println(a.equals(b)); // false | true FALSE
        System.out.println(a); // bbb | bb bb
        System.out.println(b); // bbb bbb
        b =  b + "a";
        System.out.println(b); // bbba
        System.out.println(b == "bbba"); // false
        System.out.println(b.equals("bbba")); // true

    }
}
