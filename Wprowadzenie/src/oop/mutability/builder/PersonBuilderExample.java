package oop.mutability.builder;

/**
 * Created by RENT on 2017-07-22.
 */
public class PersonBuilderExample {

    public static void main(String[] args) {
        PersonBuilder personBuilder = new PersonBuilder().withName("Adam");
        Person a = personBuilder.withSurname("Kowalski").build();
        Person b = personBuilder.withSurname("Nowak").build();
        System.out.println(a);
        System.out.println(b);
        PersonBuilder kowalski = personBuilder.withSurname("Kowalski");

        Person c = kowalski.build();
        Person d = kowalski.build();

        System.out.println(c == d);

    }
}
