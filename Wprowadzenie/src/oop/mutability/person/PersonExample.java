package oop.mutability.person;


/**
 * Created by RENT on 2017-07-22.
 */
public class PersonExample {

    public static void main(String[] args) {
        Person a = new Person("Jan", "Kowalski");
        Person b = new Person("Jan", "Kowalski");

        System.out.println(a == b); // true | false
        System.out.println(a.equals(b)); // true | true

        b.setName("Adam");
        System.out.println(a == b); // false | false
        System.out.println(a.equals(b)); // false | false
        a = b;
        System.out.println(a == b); // false | true | true
        System.out.println(a.equals(b)); // false | true |
        a.setName("Paweł");
        b.setName("Nowak");
        System.out.println(a == b);  // false | true
        System.out.println(a.equals(b)); // false | true
        System.out.println(a); // Adam Kowalski // Paweł Nowak
        System.out.println(b); // Adam Nowak // Paweł Nowak


    }
}
