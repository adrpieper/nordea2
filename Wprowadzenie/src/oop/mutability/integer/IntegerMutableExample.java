package oop.mutability.integer;

/**
 * Created by RENT on 2017-07-22.
 */
public class IntegerMutableExample {

    public static void main(String[] args) {
        IntegerMutable a = new IntegerMutable(1000);
        IntegerMutable b = new IntegerMutable(1000);
        System.out.println(a == b); // false | false | true FALSE
        System.out.println(a.equals(b)); //true | true | false TRUE
        b.setValue(400);
        a = b;
        System.out.println(a == b); //false | true TRUE
        b.setValue(300);

        System.out.println(a == b); //false | true TRUE
        System.out.println(a.equals(b)); //false | true TRUE
        System.out.println(a); //400 | 300
        System.out.println(b); //300 | 300
    }
}
