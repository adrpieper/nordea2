package oop.mutability.integer;

/**
 * Created by RENT on 2017-07-22.
 */
public class IntExample {

    public static void main(String[] args) {
        int a = 1000;
        int b = 1000;
        System.out.println(a == b);
        b = 400;
        a = b;
        b = 300;

        System.out.println(a == b);
        System.out.println(a);
        System.out.println(b);
    }
}
