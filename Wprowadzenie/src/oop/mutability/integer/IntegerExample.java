package oop.mutability.integer;

/**
 * Created by RENT on 2017-07-22.
 */
public class IntegerExample {

    public static void main(String[] args) {
        Integer a = 1000;
        Integer b = 1000;
        System.out.println(a == b);
        System.out.println(a.equals(b));
        b = 400;
        a = b;
        System.out.println(a == b);
        b = 300;

        System.out.println(a == b);
        System.out.println(a.equals(b));
        System.out.println(a);
        System.out.println(b);
    }
}
