package oop;

/**
 * Created by RENT on 2017-07-12.
 */
public class TeddyBear {
    private String name;
    public TeddyBear (String name) {
        this.name = name;
    }
    public static void main (String[] args) {
        TeddyBear[] misie = { new TeddyBear("Miś"), new TeddyBear("Puchaty"), new TeddyBear("Klapouchy") };
        for (TeddyBear teddyBear : misie){
            teddyBear.print();
        }
    }
    public void print() {
        System.out.println("Nazywam się "+ name + " i chce cię przytulić");
    }
}
