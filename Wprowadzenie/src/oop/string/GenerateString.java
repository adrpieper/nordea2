package oop.string;

/**
 * Created by RENT on 2017-07-22.
 */
public class GenerateString {

    public static void main(String[] args) {

        char[] string = "aaaa".toCharArray();
        System.out.println(concat(string, 'b'));

        String text = generate('a', 50);
        System.out.println(text);
        System.out.println(fastGenerate('a', 50));
        System.out.println(fastGenerateByBuilder('a', 50));

        long start;
        long end;
        start = System.currentTimeMillis();
        generate('a', 400);
        end = System.currentTimeMillis();
        System.out.println(end - start);
        start = System.currentTimeMillis();
        fastGenerate('a', 400);
        end = System.currentTimeMillis();
        System.out.println(end - start);
        start = System.currentTimeMillis();
        fastGenerateByBuilder('a', 400);
        end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    private static String generate(char c, int length) {
        String text = "";
        for (int i = 0; i < length; i++) {
            text += c;
        }
        return text;
    }

    public static char[] concat(char[] string, char c) {
        char[] newString = new char[string.length + 1];

        for (int i = 0; i < string.length; i++) {
            newString[i] = string[i];
        }
        newString[string.length] = c;
        return newString;

    }

    public static String fastGenerate(char c, int length) {
        char[] newString = new char[length];
        for (int i = 0; i < length; i++) {
            newString[i] = c;
        }
        return new String(newString);
    }

    public static String fastGenerateByBuilder(char c, int length) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            builder.append(c);
        }
        return builder.toString();
    }
}
