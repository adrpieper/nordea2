package oop.family;

/**
 * Created by RENT on 2017-07-12.
 */
public class Family {
    private Mother mother = new Mother("Ania");
    private Father father = new Father("Jan");
    private Daugther daugther = new Daugther("Jola");
    private Son son = new Son("Sebastian");

    private void introduce() {
        FamilyMember[] members = {mother, father, daugther, son};

        for (FamilyMember member : members) {
            member.introduce();
            if (member.isAdult()) {
                System.out.println("I'm an adult.");
            }
        }
    }

    public static void main(String[] args) {
        Family family = new Family();
        family.introduce();

    }
}
