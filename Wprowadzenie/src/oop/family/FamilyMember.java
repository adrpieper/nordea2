package oop.family;

/**
 * Created by RENT on 2017-07-13.
 */
public abstract class FamilyMember {
    private String name;
    private boolean isAdult;

    public FamilyMember(String name, boolean isAdult) {
        this.name = name;
        this.isAdult = isAdult;
    }

    public String getName() {
        return name;
    }

    public boolean isAdult() {
        return isAdult;
    }

    public abstract void introduce();
}
