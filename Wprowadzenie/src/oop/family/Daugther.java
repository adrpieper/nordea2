package oop.family;

/**
 * Created by RENT on 2017-07-12.
 */
public class Daugther extends FamilyMember {

    public Daugther(String name) {
        super(name, false);
    }

    @Override
    public void introduce() {
        System.out.println("I’m a daugther. My name is "+ getName());
    }
}
