package oop.quadratic.equation;

/**
 * Created by RENT on 2017-07-15.
 */
public class NegativeDeltaException extends RuntimeException {

    public NegativeDeltaException(double delta) {
        super("Delta is negativ ("+delta+")");
    }
}
