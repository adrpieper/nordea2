package oop.quadratic.equation;

/**
 * Created by RENT on 2017-07-12.
 */
public class QuadraticEquation {
    private double a;
    private double b;
    private double c;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calcDelta() {
        return b * b - 4 * a * c;
    }

    public double calcX1() throws NegativeDeltaException {
        if (calcDelta() > 0) {
            double x1 = 0;
            x1 = (-b - Math.sqrt(calcDelta())) / (2 * a);
            return x1;
        } else {
            throw new NegativeDeltaException(calcDelta());
        }
    }

    public double calcX2() throws NegativeDeltaException {
        if (calcDelta() > 0) {
            double x2 = 0;
            x2 = (-b + Math.sqrt(calcDelta())) / (2 * a);
            return x2;
        } else {
            throw new NegativeDeltaException(calcDelta());
        }
    }

    public static void main(String[] args) {
        QuadraticEquation equation1 = new QuadraticEquation(1, 3, 2);
        System.out.println("delta = " + equation1.calcDelta());
        if (equation1.calcDelta() > 0) {
            System.out.println("X1 = " + equation1.calcX1());
            System.out.println("X2 = " + equation1.calcX2());
        } else {
            System.out.println("Delta ujemna !!!");
        }

        QuadraticEquation equation2 = new QuadraticEquation(1, 2, 3);
        System.out.println("delta = " + equation2.calcDelta());
        if (equation2.calcDelta() > 0) {
            System.out.println("X1 = " + equation2.calcX1());
            System.out.println("X2 = " + equation2.calcX2());
        } else {
            System.out.println("Delta ujemna !!!");
        }
    }

}
