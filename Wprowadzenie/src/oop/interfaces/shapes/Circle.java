package oop.interfaces.shapes;

/**
 * Created by RENT on 2017-07-13.
 */
public class Circle implements Figure {
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    @Override
    public double countArea() {
        return Math.PI * r * r;
    }

    @Override
    public double countCircumference() {
        return Math.PI * 2 * r;
    }
}
