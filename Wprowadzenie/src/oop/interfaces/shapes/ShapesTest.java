package oop.interfaces.shapes;

/**
 * Created by RENT on 2017-07-13.
 */
public class ShapesTest {

    public static void main(String[] args) {
        Figure square = new Square(3);
        Figure circle = new Circle(3);
        print(square);
        print(circle);
    }

    public static void print(Figure figure) {
        System.out.println(figure.countArea());
        System.out.println(figure.countCircumference());
    }
}
