package oop.exceptions;

/**
 * Created by RENT on 2017-07-15.
 */
public class TryCatch {

    public static void main(String[] args) {
        System.out.println("Start");

        try{

            int a = 4/2;
            System.out.println(a);

            int b = 4/0;
            System.out.println(b);

            int c = 4/1;
            System.out.println(c);
        }finally {
            System.out.println("Finally");
        }

        System.out.println("Koniec");
    }
}
