package oop.exceptions;

import java.util.Scanner;

/**
 * Created by RENT on 2017-07-15.
 */
public class DivideABTryCatch {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj a: ");
        int a = scanner.nextInt();
        System.out.println("Podaj b: ");
        int b = scanner.nextInt();

        // Try-catch może zastąpić instrukcje if-else, ale
        // nie powinniśmy tego robić, jeżeli możemy uniknąć rzucania wyjątków.
        try {
            System.out.println(a / b + " i " + a % b + "/" + b);
        }catch (ArithmeticException arithmeticException){
            System.out.println("Dzielenie przez zero");
        }

    }
}
