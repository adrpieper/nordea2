package oop.person;

/**
 * Created by RENT on 2017-07-12.
 */
public class PersonTest {

    public static void main(String[] args) {
        Person[] people = {
                new Person("Jan", "Kowalski", 20),
                new Person("Adam", "Nowak", 24),
        };

        for (Person person : people) {
            System.out.println(person);
        }

    }
}
