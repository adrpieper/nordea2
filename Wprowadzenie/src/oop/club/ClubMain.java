package oop.club;

/**
 * Created by RENT on 2017-07-15.
 */
public class ClubMain {

    public static void main(String[] args) {
        Person jan = new Person("Jan", "Kowalski", 20);
        Person ania = new Person("Anna", "Nowak", 17);
        Person adam = new Person("Adam", "Nowak", 22);

        Club club = new Club();

        Person[] people = {jan, ania, adam};

        for (Person person : people) {
            try {
                club.enter(person);
            }catch (NoAdultException e) {
                System.out.println(person.getName() + " jest niepełnoletni.");
            }
        }

        for (Person person : people) {
            if (person.getAge() > 18) {
                club.enter(person);
            }else {
                System.out.println(person.getName() + " jest niepełnoletni.");
            }
        }
    }
}
