package oop.osoba;

/**
 * Created by RENT on 2017-07-12.
 */
public class TrzyOsoby {

    public static void main(String[] args) {

        Osoba jan = new Osoba(20, "Jan");
        Osoba adam = new Osoba(22, "Adam");
        Osoba ania = new Osoba(24, "Ania");



        Osoba[] osoby = {jan, adam, ania};
        for (Osoba osoba : osoby) {
            System.out.println("Jestem "+ osoba.getImie() + ", mam "+osoba.getWiek()+" lat.");
        }


    }
}
