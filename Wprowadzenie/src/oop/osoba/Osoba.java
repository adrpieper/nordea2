package oop.osoba;

/**
 * Created by RENT on 2017-07-12.
 */
public class Osoba {
    private int wiek;
    private String imie;

    public Osoba(int wiek, String imie) {
        this.wiek = wiek;
        this.imie = imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getWiek() {
        return wiek;
    }

    public static void main(String[] args) {

        Osoba jan = new Osoba(30, "Jan");
        Osoba adam = new Osoba(25, "Adam");

        adam.setImie("Adam2");
        adam.print();
        jan.print();

        print(adam);
        print(jan);
        System.out.println("Wiek adama : " + adam.getWiek());

    }

    public void print() {
        System.out.println(imie); // this.imie
        System.out.println(this.wiek);
    }

    static void print(Osoba osoba) {
        System.out.println(osoba.imie);
        System.out.println(osoba.wiek);
    }

    public String getImie() {
        return imie;
    }
}
