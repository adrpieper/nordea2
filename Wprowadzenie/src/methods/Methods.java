package methods;

/**
 * Created by Adrian on 2017-07-10.
 */
public class Methods {

    public static void main(String[] args) {
        printHelloWorld(2);
        printHelloWorld(4);
        printSum(5, 7);
        int ab = calcProduct(3, 12);
        System.out.println(ab);
        printSum(calcProduct(2, 3), 2);
        System.out.println(calcProduct(2 + 3, 2));

        System.out.println(min(8, 10));
        System.out.println(min(8, 10, 12));

        double[] data = {3,5,2,4,1,9};
        System.out.println(min(data));
        System.out.println(min(new double[0]));
    }

    public static void printHelloWorld() {
        System.out.println("HelloWorld !");
    }

    public static void printHelloWorld(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println("Hello world");
        }
    }

    public static void printSum(int a, int b) {
        System.out.println(a + b);
    }

    public static int calcProduct(int a, int b) {
        int result = a * b;
        return result;
    }

    public static double min(double a, double b) {
        if (a <= b) {
            return a;
        } else {
            return b;
        }
        //return (a <= b) ? a : b;
    }

    public static double min(double a, double b, double c) {
        return min(min(a, b), c);
    }

    public static double min(double a, double b, double c, double d) {
        return min(min(min(a, b), c), d);
    }

    public static double min(double[] data) {
        if (data.length == 0) {
            throw new IllegalArgumentException("Array is empty");
        }
        double currentMin = data[0];
        for (int i=1; i<data.length; i++) {
            if (data[i] < currentMin) {
                currentMin = data[i];
            }
            // currentMin = min(currentMin, data[i]);
        }

        return currentMin;
    }
}
