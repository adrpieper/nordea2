package methods;

/**
 * Created by RENT on 2017-07-11.
 */
public class Overloading {

    public static void main(String[] args) {
        show(6);
        show(6.0);
        show('a');
        show("6");
        String s = "text";
        show(s);
        Object o = s;
        show(o);
        o = 4.0;
        show(o);
    }

    public static void show(int number) {
        System.out.println("Integer : " + number);
    }

    public static void show(double number) {
        System.out.println("Double: " + number);
    }

    public static void show(String string) {
        System.out.println("String: " + string);
    }

    public static void show(Object object) {
        System.out.println("Object: " + object);
    }
}
