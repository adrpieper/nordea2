package methods;

/**
 * Created by RENT on 2017-07-11.
 */
public class Divider {

    public static void main(String[] args) {

        System.out.printf("Liczba to %.3f",1.039272789);
        System.out.println();
        System.out.println(String.format("Liczba to %.3f",1.039272789));

        printDivide(6,5);
        printDivide(6.0,5);
        printDivide(6,5.0);
        printDivide((double) 6,5);
        printDivide(6d,5d);

    }

    public static void printDivide(int a, int b) {
        System.out.println("Wynik dzielenia " + a + "/" + b + " to " + a / b + " i " + a % b + "/" + b);
    }
    public static void printDivide(double a, double b) {
        System.out.println("Wynik dzielenia " + a + "/" + b + " to " + String.format("%.3f", a / b) );
    }
}
