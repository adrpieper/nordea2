package methods;

/**
 * Created by RENT on 2017-07-11.
 */
public class Geometry {

    public static void main(String[] args) {
        System.out.println("Pole kwadratu o boku 3 :"+ areaSquare(3));
        System.out.println("Pole sześcianu o boku 3 :"+ areaCube(3));
        System.out.println("Pole koła o promieniu 3 :"+ areaCircle(3));
        System.out.println("Objętośc walca o promieniu 3 i wysokości 4 :"+ cylinderVolume(3,4));
        System.out.println("Objętośc stożka o promieniu 3 i wysokości 4 :"+ coneVolume(3,4));
        System.out.println("Objętośc sześcianu o boku 3 :"+ cubeVolume(3));
        System.out.println("Objętośc ostrosłupa o podstawie kwadratu o boku 3 i wysokości 4 :"+ pyramidVolume(3,4));

    }
    public static double areaSquare(double a){
        return a*a;
    }
    public static double areaCube(double a){
        return areaSquare(a)*6;
    }
    public static double areaCircle(double r){
        return Math.PI * r*r ;
    }
    public static double cylinderVolume(double r, double h){
        return areaCircle(r)*h;
    }
    public static double coneVolume(double r, double H){
        return cylinderVolume(r,H)/3;
    }
    public static double cubeVolume(double a){
        return areaSquare(a)*a;
    }
    public static double pyramidVolume(double a, double H){
        return (areaSquare(a)*H)/3;
    }
}
