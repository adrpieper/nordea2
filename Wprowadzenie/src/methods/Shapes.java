package methods;

/**
 * Created by Adrian on 2017-07-10.
 */
public class Shapes {

    public static void main(String[] args) {
//        printRect(2,4);
//        printRect(3,3);
//        printRect(4,2);
//        printRect(1,1);
        //printTriangle(4);
//        printRightTriangle(5);
        printFrame(4,8);
    }

    public static void printFrame(int a, int b){
        printLine(b);
        for (int i = 1; i < a-1; i++){

            System.out.print("*");
            printSpaces(b-2);
            System.out.println("*");

        }
        printLine(b);
    }

    public static void printTriangle(int height){
        for(int i=1;i<=height;i++){
            printLine(i);
        }
    }

    public static void printRightTriangle(int height){
        for(int i=1;i<=height;i++){
            printSpaces(height-i);
            printLine(i);
        }
    }

    public static void printRect(int a, int b) {
        for(int i = 0;i<a;i++){
            printLine(b);
        }
    }

    public static void printTriangle(int a,int b) {
        for (int i = 1; i <=a; i++) {
            printLine((b*i+a-1)/a);
        }
    }

    private static void printLine(int length) {
        for(int j = 0;j<length;j++){
            System.out.print("*");
        }
        System.out.println();
    }

    private static void printSpaces(int length) {
        for(int j = 0;j<length;j++){
            System.out.print(" ");
        }
    }
}
