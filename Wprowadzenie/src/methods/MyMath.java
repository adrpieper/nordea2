package methods;

/**
 * Created by RENT on 2017-07-11.
 */
public class MyMath {

    public static void main(String[] args) {
        System.out.println("abs(-8) = " + abs(-8));
        System.out.println("abs(8) = " + abs(8));
        System.out.println("abs(-8.0) = " + abs(-8.0));
        System.out.println("abs(8.0) = " + abs(8.0));
        System.out.println("pow(2,3) = " + pow(2d,3));
        System.out.println("pow(2,-3) = " + pow(2d,-3));
        System.out.println("pow(2,3) = " + pow(2,-3));

        int a = abs(-4);
    }

    public static int abs(int a) {
        return a >= 0 ? a : -a;
    }

    public static double abs(double a) {
        return a >= 0 ? a : -a;
    }

    public static int pow(int a, int b) {
        int result = 1;
        for (int i = 0; i < b; i++) {
            result *= a;
        }
        return result;
    }

    public static double pow(double a, int b) {
        if (b>=0) {
            double result = 1;
            for (int i = 0; i < b; i++) {
                result *= a;
            }
            return result;
        }
        else {
            return 1.0/pow(a,-b);
        }
    }
}
