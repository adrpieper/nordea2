package memory.twoint;

/**
 * Created by RENT on 2017-07-20.
 */
public class TwoIntsOperations {

    public static void main(String[] args) {

        int first = 5;
        int second = 2;
        double result = TwoIntsOperations.divide(first, second);
        System.out.println(result);
    }

    public static double divide(double a, double b) {
        return a / b;
    }
}
