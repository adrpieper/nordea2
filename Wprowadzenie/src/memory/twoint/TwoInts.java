package memory.twoint;

/**
 * Created by RENT on 2017-07-20.
 */
public class TwoInts {
    private int a;
    private int b;

    public TwoInts(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public static void main(String[] args) {
        TwoInts twoInts = new TwoInts(5,2);
        double result = twoInts.divide();
        System.out.println(result);
    }

    public double divide() {
        return (double) a/b;
    }
}
