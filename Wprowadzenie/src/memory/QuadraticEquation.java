package memory;

import oop.quadratic.equation.NegativeDeltaException;

/**
 * Created by RENT on 2017-07-12.
 */
public class QuadraticEquation {
    private double a;
    private double b;
    private double c;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calcDelta() {
        return b * b - 4 * a * c;
    }

    public double calcX1() throws NegativeDeltaException {
        return (-b - Math.sqrt(calcDelta())) / (2 * a);
    }

    public double calcX2() throws NegativeDeltaException {
        return  (-b + Math.sqrt(calcDelta())) / (2 * a);
    }

    public static void main(String[] args) {
        QuadraticEquation equation1 = new QuadraticEquation(1, 3, 2);
        System.out.println("delta = " + equation1.calcDelta());
        System.out.println("X1 = " + equation1.calcX1());
        System.out.println("X2 = " + equation1.calcX2());
    }

}
