package utils;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by RENT on 2017-07-27.
 */
public class TestNewArrayList {

    @Test
    public void noneElementsShouldReturnEmptyList() {

        List<Integer> actual = Utils.newArrayList();
        assertThat(actual).isEmpty();
    }


    @Test
    public void oneTwoThreeShouldReturnListWithTheseElements() {

        List<Integer> actual = Utils.newArrayList(1, 2, 3);
        assertThat(actual).containsExactly(1,2,3);
    }
}
