package utils;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by RENT on 2017-07-26.
 */
public class TestUtilsArraysEqual {
    @Test
    public void twoIdenticalArraysShouldReturnTrue() {
        int[] givenArray1 = {1, 3};
        int[] givenArray2 = {1, 3};
        boolean actual = Utils.arrayEquals(givenArray1, givenArray2);
        assertThat(actual).isTrue();
    }

    @Test
    public void twoDifferentArraysShouldReturnFalse() {
        int[] givenArray1 = {1, 3};
        int[] givenArray2 = {2, 4};
        boolean actual = Utils.arrayEquals(givenArray1, givenArray2);
        assertThat(actual).isFalse();
    }

    @Test
    public void twoArraysWhereArray1IsNullShouldReturnFalse() {
        int[] givenArray1 = null;
        int[] givenArray2 = {2, 4};
        boolean actual = Utils.arrayEquals(givenArray1, givenArray2);
        assertThat(actual).isFalse();
    }
    @Test
    public void twoArraysWhereArray2IsNullShouldReturnFalse() {
        int[] givenArray1 = {2, 4};
        int[] givenArray2 = null;
        boolean actual = Utils.arrayEquals(givenArray1, givenArray2);
        assertThat(actual).isFalse();
    }
    @Test
    public void twoArraysWhereArray1IsEmptyShouldReturnFalse() {
        int[] givenArray1 = {};
        int[] givenArray2 = {2, 4};
        boolean actual = Utils.arrayEquals(givenArray1, givenArray2);
        assertThat(actual).isFalse();
    }

    @Test
    public void twoArraysWhereArray2IsEmptyShouldReturnFalse() {
        int[] givenArray1 = {2, 4};
        int[] givenArray2 = {};
        boolean actual = Utils.arrayEquals(givenArray1, givenArray2);
        assertThat(actual).isFalse();
    }

    @Test
    public void twoArraysWhereArray1IsBiggerThanSecondShouldReturnFalse() {
        int[] givenArray1 = {2, 4, 5};
        int[] givenArray2 = {1};
        boolean actual = Utils.arrayEquals(givenArray1, givenArray2);
        assertThat(actual).isFalse();
    }

    @Test
    public void twoArraysWhereArray2IsBiggerThanSecondShouldReturnFalse() {
        int[] givenArray1 = {2};
        int[] givenArray2 = {2, 4,5};
        boolean actual = Utils.arrayEquals(givenArray1, givenArray2);
        assertThat(actual).isFalse();
    }

    @Test
    public void twoSimilarArrayInDifferentOrderShouldReturnFalse() {
        int[] givenArray1 = {2, 4};
        int[] givenArray2 = {4, 2};
        boolean actual = Utils.arrayEquals(givenArray1, givenArray2);
        assertThat(actual).isFalse();
    }
}
