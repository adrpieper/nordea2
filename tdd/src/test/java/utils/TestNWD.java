package utils;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by RENT on 2017-07-27.
 */
public class TestNWD {

    @Test
    public void fourAndEightShouldReturnTwo(){
        int expected = 4;
        int actual = Utils.nwd(4,8);
        assertThat(expected).isEqualTo(actual);
    }

    @Test
    public void sixAndEightShouldReturnTwo(){
        int expected = 2;
        int actual = Utils.nwd(6,8);
        assertThat(expected).isEqualTo(actual);
    }

    @Test
    public void theSameTwoNumbersShouldReturnThatNumber(){
        int expected = 10;
        int actual = Utils.nwd(10,10);
        assertThat(expected).isEqualTo(actual);
    }

    @Test(expected = ArithmeticException.class)
    public void oneNumberIsZeroShouldReturnArithmeticException(){
        Utils.nwd(0,10);
    }

    @Test(expected = ArithmeticException.class)
    public void oneNumberIsZero2ShouldReturnArithmeticException(){
        Utils.nwd(10,0);
    }

    @Test(expected = ArithmeticException.class)
    public void twoNumbersAreZeroShouldReturnArithmeticException(){
        Utils.nwd(0,0);
    }

    @Test
    public void twoNumbersAreNegativeShouldReturn(){
        int expected = 10;
        int actual = Utils.nwd(-10,-100);
        assertThat(expected).isEqualTo(actual);
    }

    @Test
    public void oneNumberAreNegativeShouldReturn(){
        int expected = 10;
        int actual = Utils.nwd(10,-100);
        assertThat(expected).isEqualTo(actual);
    }

    @Test
    public void oneNumberIsNegativeShouldReturn(){
        int expected = 10;
        int actual = Utils.nwd(-10,100);
        assertThat(expected).isEqualTo(actual);
    }
}
