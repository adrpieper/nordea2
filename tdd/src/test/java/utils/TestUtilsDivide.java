package utils;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;
/**
 * Created by RENT on 2017-07-26.
 */
public class TestUtilsDivide {

    @Test
    public void divideFiveByFiveShouldReturn1() {
        double expected = 1.0;
        double actual = Utils.divide(5,5);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void divideFiveByTwoShouldReturn2AndHalf() {
        double expected = 2.5;
        double actual = Utils.divide(5,2);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void divideOneByThreeShouldReturn03() {
        // expected ~ 1/3
        double actual = Utils.divide(1,3);
        assertThat(actual).isBetween(0.333333,0.333334);
    }

    @Test(expected = ArithmeticException.class)
    public void divideOneByZeroShouldThrowException() {
        Utils.divide(1, 0);
    }


}
