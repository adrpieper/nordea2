package utils;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

/**
 * Created by RENT on 2017-07-26.
 */
public class TestFivePlusFive {

    @Test
    public void fivePlusFiveShouldReturn10() {
        int expected = 10;
        int actual = 5 + 5;
        assertThat(expected).isEqualTo(actual);
    }
}
