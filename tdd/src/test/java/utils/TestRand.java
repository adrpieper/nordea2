package utils;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by RENT on 2017-07-27.
 */
public class TestRand {

    @Test
    public void nullArrayShouldReturnEmptyString() {
        String[] given = null;
        String actual = Utils.rand(given);
        assertThat(actual).isEmpty();
    }

    @Test
    public void emptyArrayShouldReturnEmptyString() {
        String[] given = {};
        String actual = Utils.rand(given);
        assertThat(actual).isEmpty();
    }

    @Test
    public void arrayWithOneElementsShouldReturnIt() {
        String[] given = {"a"};
        String actual = Utils.rand(given);
        assertThat(actual).isEqualTo("a");
    }

    @Test
    public void arrayWithTwoElementsShouldReturnOneOfThem() {
        String[] given = {"a", "b"};
        String actual = Utils.rand(given);
        assertThat(actual).isIn(given);
    }

    @Test
    public void everyElementHasChangeToBeRanded(){
        String[] given = {"a", "b"};
        Set<String> actual = new HashSet<>();

        for (int i = 0; i < 1000; i++) {
            actual.add(Utils.rand(given));
            if (actual.size() == given.length) {
                break;
            }
        }

        assertThat(actual).contains(given);
    }
}
