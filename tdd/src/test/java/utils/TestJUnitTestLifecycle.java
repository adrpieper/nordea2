package utils;

import org.junit.*;

/**
 * Created by Adrian on 2017-07-26.
 */
public class TestJUnitTestLifecycle {

    public static void main(String[] args) {
        beforeClass();
        TestJUnitTestLifecycle underTest;
        underTest = new TestJUnitTestLifecycle();
        underTest.before();
        underTest.firstTest();
        underTest.after();
        underTest = new TestJUnitTestLifecycle();
        underTest.before();
        underTest.secondTest();
        underTest.after();
        afterClass();

    }

    public TestJUnitTestLifecycle() {
        System.out.println("Konstruktor");
    }

    @BeforeClass
    public static void beforeClass() {
        System.out.println("BeforeClass");
    }

    @Before
    public void before() {
        System.out.println("Before");
    }

    @Test
    public void firstTest(){
        System.out.println("firstTest");
    }

    @Test
    public void secondTest(){
        System.out.println("secondTest");
    }

    @After
    public void after() {
        System.out.println("After");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("AfterClass");
    }

}
