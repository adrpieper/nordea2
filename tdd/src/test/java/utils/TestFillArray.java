package utils;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by RENT on 2017-07-27.
 */
public class TestFillArray {

    @Test
    public void tenElementsArrayWillBeFilledByOnes() {
        int [] actual = new int[10];
        Utils.fillArray(actual, 1);
        assertThat(actual).containsOnly(1);
    }

    @Test
    public void emptyArrayDoNothing() {
        int [] actual = {};
        Utils.fillArray(actual, 1);
    }

    @Test(expected = NullPointerException.class)
    public void nullArrayShouldThrowNullPointerException() {
        int [] actual = null;
        Utils.fillArray(actual, 1);
    }
}
