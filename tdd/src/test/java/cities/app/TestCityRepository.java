package cities.app;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by RENT on 2017-07-29.
 */
public class TestCityRepository {

    private CityRepository underTest = new CityRepository();

    @Test
    public void whenRepositoryIsEmptyGetShouldReturnNull() {
        City actual = underTest.get(1);
        assertThat(actual).isNull();
    }

    @Test
    public void whenAddCityToRepositoryGetShouldReturnThisCity() {
        long cityId = underTest.add("CityName", 1000, 1000.0);

        City actual = underTest.get(cityId);
        assertThat(actual).isEqualToComparingFieldByField(new City(cityId, "CityName", 1000, 1000.0));
    }

    @Test
    public void whenAddNoneCitiesGetByNameShouldReturnEmptyList() {
        List<City> actual = underTest.getCitiesByName("CityName");
        assertThat(actual).isEmpty();
    }

    @Test
    public void whenAddOneCityGetByNameShouldReturnThisCity() {
        long cityId = underTest.add("CityName", 1000, 1000.0);
        List<City> actual = underTest.getCitiesByName("CityName");
        assertThat(actual).containsOnly(new City(cityId, "CityName", 1000, 1000.0));
    }

    @Test
    public void whenFewCitiesWithTheSameNameGetByNameShouldReturnTheseCities() {
        long cityId1 = underTest.add("CityName", 1000, 1000.0);
        long cityId2 = underTest.add("CityName", 1000, 1000.0);
        long cityId3 = underTest.add("CityName", 1000, 1000.0);
        List<City> actual = underTest.getCitiesByName("CityName");
        assertThat(actual).containsOnly(
                new City(cityId1, "CityName", 1000, 1000.0),
                new City(cityId2, "CityName", 1000, 1000.0),
                new City(cityId3, "CityName", 1000, 1000.0)
        );
    }

    @Test
    public void checkingIfTheTwoCitiesAreTheSameIgnoringID() {
        long cityCheck = underTest.add("Kraków", 100, 100);
        long cityCheckTwo = underTest.add("Kraków", 100, 100);
        City actual = underTest.get(cityCheck);
        City other = underTest.get(cityCheckTwo);
        assertThat(actual).isEqualToIgnoringGivenFields(other, "id");
    }

    @Test
    public void whenCitiesHaveTheSameNameIdAreDifferent() {
        long cityId1 = underTest.add("Sopot", 900, 339.8);
        long cityId2 = underTest.add("Sopot", 99800, 33569.8);
        List<City> cities = underTest.getCitiesByName("Sopot");

        assertThat(cityId1).isNotEqualTo(cityId2);
        assertThat(cities.get(0).getId()).isNotEqualTo(cities.get(1).getId());
    }

    @Test
    public void deleteExistingCityShouldRemoveOnlyThisCity() {
        long givenCityId = underTest.add("Sopot", 900, 339.8);
        long cityId = underTest.add("Sopot", 900, 339.8);

        boolean actual = underTest.delete(cityId);
        assertThat(actual).isTrue();
        assertThat(underTest.get(cityId)).isNull();
        assertThat(underTest.getCitiesByName("Sopot"))
                .containsOnly(new City(givenCityId,"Sopot", 900, 339.8));
    }

    @Test
    public void deleteNoExistingCityShouldFail() {
        boolean actual = underTest.delete(5L);
        assertThat(actual).isFalse();
    }

    @Test
    public void updateNoExistingCityShouldFail() {
        boolean actual = underTest.update(new City(1L,"Sopot", 900, 339.8));
        assertThat(actual).isFalse();
    }

    @Test
    public void updateExistingCityShouldUpdateOnlyThisCity() {
        long givenCityId = underTest.add("Sopot", 900, 339.8);
        long sopotId = underTest.add("Sopot", 900, 339.8);
        City updatedSopot = new City(sopotId, "Sopot2", 21, 100.0);

        boolean actual = underTest.update(updatedSopot);

        assertThat(actual).isTrue();
        assertThat(underTest.get(sopotId)).isEqualToComparingFieldByField(updatedSopot);
        assertThat(underTest.get(givenCityId)).isEqualToComparingFieldByField(new City(givenCityId,"Sopot", 900, 339.8));
        assertThat(underTest.getCitiesByName("Sopot")).containsOnly(new City(givenCityId,"Sopot", 900, 339.8));
    }

    @Test
    public void whenModifyGettedCityRepositoryIsNotModify() {
        long sopotId = underTest.add("Sopot", 900, 339.8);
        City sopot = underTest.get(sopotId);
        sopot.setId(100L);
        sopot.setName("Sopot2");
        sopot.setArea(0);
        sopot.setCitizens(100);

        assertThat(underTest.get(sopotId)).isEqualToComparingFieldByField(new City(sopotId,"Sopot", 900, 339.8));
    }

    @Test
    public void whenModifyGettedByNameCityRepositoryIsNotModify() {
        long sopotId = underTest.add("Sopot", 900, 339.8);
        City sopot = underTest.getCitiesByName("Sopot").get(0);
        sopot.setId(100L);
        sopot.setName("Sopot2");
        sopot.setArea(0);
        sopot.setCitizens(100);

        assertThat(underTest.get(sopotId)).isEqualToComparingFieldByField(new City(sopotId,"Sopot", 900, 339.8));
    }
}
