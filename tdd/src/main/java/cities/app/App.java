package cities.app;

import java.util.Scanner;

/**
 * Created by RENT on 2017-07-29.
 */
public class App {

    private boolean running;


    public static void main(String[] args) {
        App app = new App();
        app.startApp();
    }

    public void startApp() {

        CityRepository cityRepository = new CityRepository();
        UserInterface userInterface = new UserInterface(UserInterface.polishTranslation());
        CommandExecutor executor = new CommandExecutor(userInterface, cityRepository, this);
        CommandParser commandParser = new CommandParser(executor);
        cityRepository.add("Gdańsk", 100, 10.0);

        running = true;
        Scanner scanner = new Scanner(System.in);
        while (running) {
            userInterface.showMessage(UserInterface.Message.NEXT_COMMAND);
            String command = scanner.nextLine();
            commandParser.parse(command);
        }
    }


    public void closeApp() {
        running = false;
    }
}
