package cities.app;

/**
 * Created by RENT on 2017-07-29.
 */
public class CommandParser {

    private final CommandExecutor commandExecutor;

    public CommandParser(CommandExecutor commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    public void parse(String command) {

        String[] split = command.split(" ");
        String commandType = split[0];

        switch (commandType){
            case "exit":
                commandExecutor.closeApp();
                break;
            case "add":

                break;
            case "getByName":
                commandExecutor.getByName(split[1]);
                break;
        }
    }
}
