package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by RENT on 2017-07-26.
 */
public class Utils {

    public static double divide(int a, int b) {
        if (b == 0) {
            throw new ArithmeticException("Can't divide by zero");
        }
        return 1.0*a/b;
    }

    public static boolean arrayEquals(int[] array1, int[] array2) {
        if (array1 == array2) {
            return true;
        }
        if (array1 == null || array2 == null) {
            return false;
        }
        if (array1.length != array2.length) {
            return false;
        }
        int length = array1.length;
        for (int i = 0; i < length; i++) {
            if (array1[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }

    public static String rand(String[] data) {
        if (data == null || data.length == 0){
            return "";
        }
        Random random = new Random();
        int index = random.nextInt(data.length);
        String word = data[index];
        return word;
    }

    public static int nwd (int a, int b){
        if(a==0 || b==0){
            throw new ArithmeticException("One number is 0");
        }else if(a<0 || b<0){
            a=Math.abs(a);
            b=Math.abs(b);
        }
        while (a != b){
            if (a > b) {
                a = a - b;
            }
            else {
                b = b - a;
            }
        }
        return a;
    }

    public static void fillArray(int[] array, int element) {
        for (int i = 0; i <array.length ; i++) {
            array[i]=element;
        }
    }

    public static  List<Integer> newArrayList(int... elements) {
        List<Integer> list = new ArrayList<>();
        for (int element : elements) {
            list.add(element);
        }
        return list;
    }
}
