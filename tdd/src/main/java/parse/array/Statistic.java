package parse.array;

import java.util.List;

import static java.lang.Double.NaN;

/**
 * Created by RENT on 2017-07-27.
 */
public class Statistic {

    public static double mean(List<Integer> list) {

        double sum = 0;

        if (list.size() == 0) {
            return 0;
        }

        for(int integer : list){
            sum+=integer;
        }
        double average = sum/list.size();
        return average;
    }
}
