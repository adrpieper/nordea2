package parse.array;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-07-27.
 */
public class MyParseArray {


    public static List<Integer> parseList(String text) throws NumberFormatException, IllegalArgumentException{
        List<Integer> integerList = new ArrayList<>();
        if (text.equals( "[]" ) ) {
            return integerList;
        } else if (text.startsWith( "[" ) && text.endsWith( "]" )) {
            text = text.replace( "[", "" );
            text = text.replace( "]", "" );
            text = text.replace( " ", "" );
            String[] tab = text.split( "," );
            for (String s : tab) {
                integerList.add( Integer.parseInt( s ) );
            }
        } else {
            throw new IllegalArgumentException( "Brak nawiasów []" );
        }
        return integerList;
    }
}
